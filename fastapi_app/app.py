"""
Main application module
"""

from contextlib import asynccontextmanager

from fastapi import FastAPI
from fastapi.responses import JSONResponse
from sqlalchemy import Result, asc, desc, select, update
from typing_extensions import AsyncIterator, Never, NoReturn, Sequence

from .db import engine, session
from .models import Base, Recipe
from .schemas import Message, RecipeCreate, RecipeDetailsOut, RecipeNoDetailsOut


@asynccontextmanager
async def lifespan(fastapi_app: FastAPI) -> AsyncIterator[Never] | NoReturn:
    """
    Life span of FastAPI app.
    """
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
        await session.commit()

    yield

    await session.close()
    await engine.dispose()


app = FastAPI(lifespan=lifespan)


@app.post("/recipes/new_recipe", response_model=RecipeNoDetailsOut)
async def add_recipe(recipe: RecipeCreate) -> Recipe:
    """
    Endpoint to add new recipe into database
    """
    recipe = Recipe(**recipe.model_dump())
    async with session.begin():
        session.add(recipe)
    return recipe


@app.get(
    "/recipes",
    response_model=list[RecipeNoDetailsOut],
    responses={404: {"model": Message}},
)
async def get_recipes() -> Sequence[Recipe] | JSONResponse:
    """
    Endpoint to get all recipes from database, sorted by views descending.
    """
    async with session.begin():
        recipes: Result[tuple[Recipe]] = await session.execute(
            select(Recipe).order_by(desc(Recipe.views), asc(Recipe.cook_time))
        )
    result: Sequence[Recipe] = recipes.scalars().all()
    if result:
        return result
    return JSONResponse(
        status_code=404,
        content={"detail": "No recipes found. Database is empty"},
    )


@app.get(
    "/recipes/{recipe_id}",
    response_model=RecipeDetailsOut,
    responses={404: {"model": Message}},
)
async def view_recipe_details(recipe_id: int) -> list[Recipe] | JSONResponse:
    """
    Endpoint to get recipe by id.
    """
    async with (session.begin()):
        query = (
            update(Recipe)
            .values({Recipe.views: Recipe.__table__.c.views + 1})
            .filter(Recipe.id == recipe_id)
        )
        await session.execute(query)
        result = await session.execute(select(Recipe).where(Recipe.id == recipe_id))
        recipe = result.scalar_one_or_none()
    if recipe:
        return recipe
    return JSONResponse(
        status_code=404,
        content={"detail": "Recipe not found"},
    )


@app.delete(
    "/recipes/delete/{recipe_id}",
    responses={
        404: {"model": Message},
        200: {"model": Message},
    },
)
async def delete_recipe(recipe_id: int) -> JSONResponse:
    """
    Endpoint to delete recipe by id.
    """
    async with session.begin():
        result = await session.execute(select(Recipe).where(Recipe.id == recipe_id))
        recipe = result.scalar_one_or_none()
        if not recipe:
            return JSONResponse(
                status_code=404, content={"detail": "Recipe does not exist"}
            )
        await session.delete(recipe)
        return JSONResponse(
            status_code=200,
            content={"detail": "Recipe deleted"},
        )
