"""
Module of sqlalchemy models for app.py
"""

from sqlalchemy import Column, Integer, SmallInteger, String

from .db import Base


class Recipe(Base):
    """
    Recipe model for interaction with database
    """

    __tablename__ = "recipes"
    id = Column(Integer(), primary_key=True, autoincrement=True)
    name = Column(String(50, collation="NOCASE"), nullable=False)
    ingredients = Column(String(collation="NOCASE"), nullable=False)
    description = Column(String(collation="NOCASE"), default="", nullable=False)
    cook_time = Column(SmallInteger(), nullable=False)
    views = Column(Integer(), nullable=False, default=0)

    def __repr__(self):
        return f"id: {self.id}, name: {self.name}, cook time:{self.cook_time}m"
