"""
Database working module
"""

from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine
from sqlalchemy.orm import declarative_base

DATABASE = "sqlite+aiosqlite:///./cook_book.db"

engine = create_async_engine(DATABASE, echo=True)


async_session = async_sessionmaker(
    engine,
    expire_on_commit=False,
)

session = async_session()

Base = declarative_base()
