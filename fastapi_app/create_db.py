"""
Database initiation module
"""

import asyncio

from .db import engine, session
from .models import Base


async def create_db():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
        await session.commit()


asyncio.run(create_db())
