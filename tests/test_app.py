"""
Module of tests for pytest
"""
import asyncio

from fastapi.testclient import TestClient

from fastapi_app.app import app
from fastapi_app.create_db import create_db

asyncio.run(create_db())
client = TestClient(app)


FAKE_RECIPE = {
    "name": "Carbonara",
    "cook_time": 10,
    "ingredients": "Racoon butt - 1kg, Cream - 300ml",
    "description": "TASTY AF",
}



# Test for empty db. Run ONLY AFTER CREATION OF NEW EMPTY DB.
def test_get_recipes_empty_db():
    """
    Testing get all recipes route, when DB is empty
    """
    response = client.get("/recipes")
    assert response.status_code == 404
    assert response.json() == {"detail": "No recipes found. Database is empty"}


def test_create_recipe():
    """
    Testing creation of new recipe with valid data
    """
    response = client.post(
        "/recipes/new_recipe",
        json=FAKE_RECIPE,
        headers={"Content Type": "application/json"},
    )
    assert response.status_code == 200
    response = response.json()
    assert isinstance(response["id"], int)
    FAKE_RECIPE.update({"id": response["id"]})
    assert response["name"] == FAKE_RECIPE["name"]
    assert response["cook_time"] == FAKE_RECIPE["cook_time"]
    assert response["views"] == 0
    FAKE_RECIPE.update({"views": response["views"]})


def test_create_recipe_invalid_data():
    """
    Testing creation of new recipe with invlaid data
    """
    response = client.post(
        "/recipes/new_recipe",
        json={"poop": "bloop"},
        headers={"Content Type": "application/json"},
    )
    assert response.status_code == 422


def test_get_recipe_not_exists():
    """
    Testing get recipe by id route with non-existent recipe id
    """
    response = client.get("/recipes/-1")
    assert response.status_code == 404
    assert response.json() == {"detail": "Recipe not found"}


def test_get_all_recipes():
    """
    Testing get all recipes route with non-empty DB
    """
    response = client.get("/recipes")
    assert response.status_code == 200
    response = response.json()
    local_fake_recipe = dict(FAKE_RECIPE)
    local_fake_recipe.pop("ingredients")
    local_fake_recipe.pop("description")
    assert local_fake_recipe in response


def test_get_recipe():
    """
    Testing get recipe by id route with valid data
    """
    response = client.get(f"/recipes/{FAKE_RECIPE['id']}")
    assert response.status_code == 200
    response = response.json()
    assert response["id"] == FAKE_RECIPE["id"]
    assert response["name"] == FAKE_RECIPE["name"]
    assert response["ingredients"] == FAKE_RECIPE["ingredients"]
    assert response["description"] == FAKE_RECIPE["description"]
    assert response["cook_time"] == FAKE_RECIPE["cook_time"]
    assert response["views"] == FAKE_RECIPE["views"] + 1


def test_get_recipe_invalid_data():
    """
    Testing get recipe by id route with invalid data
    """
    response = client.get("/recipes/LOLKEK")
    assert response.status_code == 422


def test_delete_recipe():
    """
    Testing delete recipe route with valid data
    """
    response = client.delete(f"/recipes/delete/{FAKE_RECIPE['id']}")
    assert response.status_code == 200
    assert response.json() == {"detail": "Recipe deleted"}


def test_delete_recipe_not_exists():
    """
    Testing delete recipe route with non-existent recipe id
    """
    response = client.delete("/recipes/delete/-1")
    assert response.status_code == 404
    assert response.json() == {"detail": "Recipe does not exist"}


def test_delete_recipe_invalid_data():
    """
    Testing delete recipe route with invalid data
    """
    response = client.delete("/recipes/delete/LOLKEK")
    assert response.status_code == 422
